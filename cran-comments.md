## Test environments
* Linux vodina 5.15.0-43-generic, R version 4.1.2
* Ubuntu Linux 20.04.1 LTS, R-release, GCC
* Windows Server 2022, R-devel, 64 bit
* Fedora Linux, R-devel, clang, gfortran

## R CMD check results
There were no ERRORs, WARNINGs

1 NOTE:

```r
── vocaldia 0.8.4: NOTE

  Build ID:   vocaldia_0.8.4.tar.gz-b138c4ffe23747d0a75fdffa91f0ded5
  Platform:   Windows Server 2022, R-devel, 64 bit
  Submitted:  4m 53s ago
  Build time: 4m 34.6s

❯ checking for detritus in the temp directory ... NOTE
  Found the following files/directories:
    'lastMiKTeXException'
```

I believe this note is due to an error in miktex on the windows VM
used for checking.

